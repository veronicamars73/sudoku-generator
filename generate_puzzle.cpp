// Programa de impressão de texto.
#include <random>
#include <algorithm>
#include <iostream>
#include "Resolve.h"
#include <time.h>



int main(){
  int i, j;
  int tab[SIZE][SIZE];
  // Print do tabuleiro
  std::random_device rd;
  std::mt19937 g(rd());
  int possibilidades[9]={1,2,3,4,5,6,7,8,9};
  std::shuffle(std::begin(possibilidades), std::end(possibilidades), g);
  srand(time(0));
  j = rand()%8;

  for(i = 0; i < SIZE; i++){
    tab[i][j] = possibilidades[i];
  }
  printTab(tab);
  // Solução
  resolve_tabuleiro(tab);
  // print solução
  std::cout << "Solução:" << std::endl;
  printTab(tab);
  return 0; // indica que o programa terminou com sucesso
} // fim da função main
