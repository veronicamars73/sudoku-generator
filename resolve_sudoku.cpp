#include <iostream>
#include "Resolve.h"

/*! \brief Procura espaço vazio.
*
*
* Função que procura 0 (espaços vazios) no tabuleiro, caso os encontre retorna
* Verdadeiro caso contrário retorna Falso.Usada na função resolve().
* \param tabuleiro recebe o tabuleiro.
* \param linha endereço da variável linha.
* \param coluna endereço da variável coluna.
*/
void printTab( int b[SIZE][SIZE] )
{
    int i, j, contc=1, contl=1;
    std::cout << "Tabuleiro:" << std::endl;
    for (i = 0; i < SIZE; i++) {
        std::cout << "| ";
        for (j = 0; j< SIZE; j++){
            std::cout << b[i][j] << " ";

            if (contc==3){
                std::cout << "| ";
                contc = 0;
            }
            contc = contc + 1;
        }
        if (contl==3){
            std::cout << std::endl;
            contl = 0;
        }
        contl = contl + 1;
        std::cout << std::endl;
    }
}

bool encontrazero(int tabuleiro[SIZE][SIZE], int* linha, int* coluna){
  int i, k;
  for(i = 0; i<SIZE; i++){
    for(k = 0; k<SIZE; k++){
      if(tabuleiro[i][k]==0){
        *linha = i;
        *coluna = k;
        return true;
      }
    }
  }
  return false;
}

/*! \brief Analisa se um número está em uma linha.
*
*
* Função que procura, na linha fornecida, o número fornecido e retorna Verdade se
* o número existir e Falso se não.
* \param num recebe o número que deve ser procurado
* \param tabuleiro recebe o tabuleiro.
* \param linha recebe linha em que o número deve ser procurado.
*/
bool tanalinha(int num, int tabuleiro[SIZE][SIZE], int linha){
  int i;
  for (i = 0; i < SIZE; i++) {
    if(tabuleiro[linha][i]==num){
      return true;
    }
  }
  return false;
}
/*! \brief Analisa se um número está em uma coluna.
*
*
* Função que procura, na coluna fornecida, o número fornecido e retorna Verdade se
* o número existir e Falso se não.
* \param num recebe o número que deve ser procurado
* \param tabuleiro recebe o tabuleiro.
* \param coluna recebe coluna em que o número deve ser procurado.
*/
bool tanacoluna(int num, int tabuleiro[SIZE][SIZE], int coluna){
  int i;
  for (i = 0; i < SIZE; i++) {
    if(tabuleiro[i][coluna]==num){
      return true;
    }
  }
  return false;
}

/*! \brief Analisa se um número está em uma submatriz.
*
*
* Função que realiza o cálculo da submatriz da célula e procura o número dentro
* da submatriz
* \param num recebe o número que deve ser procurado
* \param tabuleiro recebe o tabuleiro.
* \param linha recebe linha da célula.
* \param coluna recebe a coluna da célula.
*/
bool tanamat(int num, int tabuleiro[SIZE][SIZE], int linha, int coluna){
  int i, k;
  linha = (linha / 3)*3;
  coluna = (coluna / 3)*3;
  for(i = linha; i<linha+3; i++){
    for(k = coluna; k<coluna+3; k++){
      if(tabuleiro[i][k]==num){
        return true;
      }
    }
  }
  return false;
}
/*! \brief Resolve o tabuleiro fornecido.
*
*
* Função que realiza a resolução do tabuleiro fornecido dentro do main
* \param tabuleiro recebe o tabuleiro.
*/
bool resolve_tabuleiro(int tabuleiro[SIZE][SIZE]){
  int i,linha, coluna;
  if (!(encontrazero(tabuleiro,&linha, &coluna))) {
    return true;
  }else{
    for(i=0; i<10;i++){
      if((!tanalinha(i,tabuleiro,linha))&&(!tanacoluna(i,tabuleiro,coluna))&&(!tanamat(i,tabuleiro,linha,coluna))){
        tabuleiro[linha][coluna] = i;
        if(resolve_tabuleiro(tabuleiro)){
          //printTab(tabuleiro);
          return true;
        }else{
          tabuleiro[linha][coluna] = 0;
        }
      }
    }
    return false;
  }
}
