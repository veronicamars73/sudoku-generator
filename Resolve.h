#ifndef RESOLVE_H
#define RESOLVE_H
#define SIZE 9

void printTab(int b[SIZE][SIZE]);
bool encontrazero(int tabuleiro[SIZE][SIZE], int* linha, int* coluna);
bool tanalinha(int num, int tabuleiro[SIZE][SIZE], int linha);
bool tanacoluna(int num, int tabuleiro[SIZE][SIZE], int coluna);
bool tanamat(int num, int tabuleiro[SIZE][SIZE], int linha, int coluna);
bool resolve_tabuleiro(int tabuleiro[SIZE][SIZE]);

#endif
